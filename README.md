﻿ 
# EaccoPHP_school_set_new

## Star一波呗！(#^.^#)

#### 项目介绍
这是一个开源项目，但是我们不放弃商用权利。  
我们使用了 EaccoPHP 的源码。 

#### 软件架构
软件架构说明


#### 安装教程

##### 下载安装环境

1. 安装git 
2. 建立空目录 EaccoPHP ，在此目录中运行 git clone https://gitee.com/XueYuanWangZhan/EaccoPHP_school_set_new.git
3. 安装 composer 并配置环境变量，这是安装文件链接：https://getcomposer.org/Composer-Setup.exe
4. 在 EaccoPHP 目录下运行  composer update

* （如果你使用 xampp 可以跳过 5-11 步，但是我不保证配置正确，自行摸索吧）

5. 下载 PHP7.1.17 https://windows.php.net/downloads/releases/php-7.1.17-Win32-VC14-x86.zip 并解压

##### 修改 apache 下的conf/httpd.conf

6. 在 apache 中配置 PHP7 （！！！注意，配置完成后，把PHP目录下的 libeay32.dll libssh2.dll ssleay32.dll 复制到 apache 的 bin 目录）
7. 在 apache 中配置网站的发布路径（ 一直写到到 EaccoPHP的public 目录（建议使用绝对路径））

##### 修改 php 目录 php.ini 

7. 修改 extension_dir 为 PHP 目录下的 ext 的绝对路径（！！！）
8. （暂时先跳过） 去掉几个注释符号 （;） 这一点你会在网站安装的时候看到

##### 安装EaccoPHP

9. 启动/重启 apache
10. 访问localhost 进入会进入安装EaccoPHP的程序
11. 如果有黄灯提示，跳的第 8 步，在 ini 中查找所需拓展并去掉分号

##### 安装调试环境

12. 如果你愿意，可以安装php——xdebug 拓展来调试程序  https://xdebug.org/files/php_xdebug-2.7.0alpha1-7.1-vc14.dll
13. 我用的开发环境是 VS code，如果需要，自行百度 “VS code php_xdebug” ，我不想再写了。


#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

  


---
---



EacooPHP v1.2.4
===============
### 介绍
EacooPHP是基于ThinkPHP5.0.15开发的一套轻量级WEB产品开发框架，追求高效，简单，灵活。
具有完善并灵活的模块化和插件机制，模块式开发，大大降低开发成本。

>支持EacooPHP的用户请给我们一个star

使用EacooPHP框架开发定制您的系统前，建议熟悉官方的tp5.0完全开发手册。

![eacoophp封面图](https://github.com/fengdou902/EacooPHP/blob/master/screenshot.jpeg)

### 功能特性
- **严谨规范：** 提供一套有利于团队协作的结构设计、编码、数据等规范。
- **高效灵活：** 清晰的分层设计、钩子行为扩展机制，解耦设计更能灵活应对需求变更。
- **严谨安全：** 清晰的系统执行流程，严谨的异常检测和安全机制，详细的日志统计，为系统保驾护航。
- **构建器Builder：** 完善的构建器设计，丰富的组件，让开发列表和表单更得心应手。无需模版开发，省时省力。
- **简单上手快：** 结构清晰、代码规范、在开发快速的同时还兼顾性能的极致追求。
- **自身特色：** 权限管理、组件丰富、第三方应用多、分层解耦化设计和先进的设计思想。
- **高级进阶：** 分布式、负载均衡、集群、Redis、分库分表。 

### 为什么选择EacooPHP框架？
**1.问：我的前端水平一般，使用EacooPHP会不会比较麻烦？**

答：EacooPHP的设计架构注重开发的高效灵活并保持性能高效，基于Builder构建器开发表单和列表，代码量非常少，后台的列表和表单简单构建，而且这个过程不需要创建view层模版文件，功能非常强大。

**2.问：我对ThinkPHP3.2/5.0有基础，学习EacooPHP容易上手开发项目吗？**

答：EacooPHP框架是基于ThinkPHP5开发的一款框架，结合tp5文档和本文档一起学习会比较容易上手。而且该框架独有开发设计，是您不错的选择。

**3.问：我们的系统功能多、体系复杂、需求变化也多，担心出现性能问题和代码维护不变！**

答：EacooPHP框架提供一套开发规范利于团队协作，系统执行流程清晰，代码结构分层设计维护方便，逻辑解耦。并且分布式、负载均衡、Redis、缓存等都有文档说明。

### 用法
例：创建一个列表页面
```
// 获取所有用户
$map =[
	'status'=> ['egt', '0'], // 禁用和正常状态
];
list($data_list,$total) = model('common/User')->search('username|nickname')->getListByPage($map,true,'create_time desc',12);

$reset_password = [
    'icon'=> 'fa fa-recycle',
    'title'=>'重置原始密码',
    'class'=>'btn btn-default ajax-table-btn confirm btn-sm',
    'confirm-info'=>'该操作会重置用户密码为123456，请谨慎操作',
    'href'=>url('resetPassword')
];
        
return builder('List')
        ->setMetaTitle('用户管理') // 设置页面标题
        ->addTopButton('addnew')  // 添加新增按钮
        ->addTopButton('delete')  // 添加删除按钮
        ->addTopButton('self',$reset_password)  // 添加重置按钮
        ->setSearch('custom','请输入关键字')//自定义搜索框
        ->keyListItem('uid', 'UID')
        ->keyListItem('avatar', '头像', 'avatar')
        ->keyListItem('nickname', '昵称')
        ->keyListItem('username', '用户名')
        ->keyListItem('email', '邮箱')
        ->keyListItem('mobile', '手机号')
        ->keyListItem('reg_time', '注册时间')
        ->keyListItem('allow_admin', '允许进入后台','status')
        ->keyListItem('status', '状态', 'array',[0=>'禁用',1=>'正常',2=>'待验证'])
        ->keyListItem('right_button', '操作', 'btn')
        ->setListPrimaryKey('uid')//设置主键uid（默认id）
        ->setExtraHtml($extra_html)//自定义html
        ->setListData($data_list)    // 数据列表
        ->setListPage($total,12) // 数据列表分页
        ->addRightButton('edit') //添加编辑按钮
        ->addRightButton('delete')  // 添加编辑按钮
        ->fetch();
```
### 效果图
![效果图](https://github.com/fengdou902/EacooPHP/blob/dev/builder-list-user-demo1.jpg)

### 前端组件
artTemplate(JS模版引擎),artDialog(弹窗),datetimepicker(日期),echarts(图表),colorpicker(颜色选择器),fastclick,iCheck(复选框美化),ieonly,imgcutter,jquery-repeater,lazyload(延迟加载),select2,superslide,ueditor,wangeditor,webuploader,x-editable

官网：[http://www.eacoo123.com](http://www.eacoo123.com)
QQ群: 436491685
### 演示地址
[http://demo1.eacoo123.com/admin](http://demo1.eacoo123.com/admin)  
账号：admin  
密码：123456 

### 项目地址
(记得给项目加个star哦)  
码云gitee：[https://gitee.com/ZhaoJunfeng/EacooPHP.git](https://gitee.com/ZhaoJunfeng/EacooPHP.git)  
GitHub：[https://github.com/fengdou902/EacooPHP.git](https://github.com/fengdou902/EacooPHP.git)  

### 鸣谢
感谢以下的项目,排名不分先后
[ThinkPHP](http://www.thinkphp.cn)、[JQuery](http://jquery.com/)、[Bootstrap](http://getbootstrap.com/)、[AdminLTE](https://almsaeedstudio.com)、[Select2](https://github.com/select2/select2)等优秀开源项目。
### 版权申明
EacooPHP遵循Apache2开源协议发布，并提供免费使用。  
本项目包含的第三方源码和二进制文件之版权信息另行标注。  
版权所有Copyright © 2017-2018 by EacooPHP (http://www.eacoo123.com)  
All rights reserved。



